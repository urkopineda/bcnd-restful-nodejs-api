# Blockchain Nanodegree - RESTful Web API with Node.js Framework

Welcome to my project! This Node.js + Express server hosts a private blockchain in which you can add blocks to it or view them.

Run the server using `npm start`.

Here are the available targets on the server:

*   **GET A BLOCK:** Make a GET request to [getting block target](block/0) adding the block height as parameter.

*   **POST A BLOCK:** Make a POST request to [adding block target](block/) adding as POST the block data (Example: `{"body": "write here some data"}`)

You can always test these targets using the test command on the repo executing `npm test`.

Regards!

Urko
