// Imports
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const Blockchain = require('./src/simpleChain').Blockchain;
const Block = require('./src/simpleChain').Block;
// Constant values
const port = process.env.PORT || 8000;
const blockchain = new Blockchain();

// Set body parser to get POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
// Welcome page
app.get('/', (req, res) => res.sendFile(`${__dirname}/index.html`));
// Get a block given its height
app.get('/block/:height', async (req, res) => {
  try {
    let height = await blockchain.getBlockHeight();
    if (height === -1) {
      res.status(500);
      res.send('ERROR: At this moment, there is no block on the chain, try adding one');
    } else {
      let block = await blockchain.getBlock(req.params.height);
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    }
  } catch (error) {
    res.status(500);
    res.send(`ERROR: ${error}`);
  }
});
// Post a block
app.post('/block', async (req, res) => {
  try {
    let data = req.body.body;
    if (data && data !== '') {
      let block = await blockchain.addBlock(new Block(data));
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    } else {
      res.status(500);
      res.send('ERROR: You must POST a JSON with the key \'body\' with some value');
    }
  } catch (error) {
    res.status(500);
    res.send(`ERROR: ${error}`);
  }
});
// Start Express server...
app.listen(port, () => console.info(`Project API listening on port ${port}...`));
