const chai = require('chai');
const expect = chai.expect;

const Blockchain = require('../src/simpleChain').Blockchain;
const Block = require('../src/simpleChain').Block;
const LevelSandbox = require('../src/levelSandbox');

const BLOCKS_TO_ADD = 10;

describe('Testing the private blockchain', () => {
  let blockchain;

  before(() => {
    blockchain = new Blockchain();
    expect(blockchain).to.not.be.null;
  });

  describe('generation', () => {
    it(`should add ${BLOCKS_TO_ADD} blocks to the chain`, async () => {
      // NOTE: When creating the Blockchain object, adds the first block; so,
      // we have to expect to be BLOCKS_TO_ADD + 1 blocks
      for (let i = 1; i <= BLOCKS_TO_ADD; i++) {
        await blockchain.addBlock(new Block(`Testing data number ${i}`));
      }
      let lastBlock = await blockchain.getBlock(BLOCKS_TO_ADD);
			expect(lastBlock.body).to.equal(`Testing data number ${BLOCKS_TO_ADD}`);
    });
  });

  describe('validation', () => {
    it(`should validate the chain`, async () => {
			let result = await blockchain.validateChain();
      expect(result).to.equal(true);
    });
  });

  describe('changing the chain', () => {
    it(`should change chain values to induce a chain validation error`, async () => {
      let inducedErrorBlocks = [2, 4, 7];
      for (let i of inducedErrorBlocks) {
        let block = await blockchain.getBlock(i);
        block.data = 'Induced chain error';
        expect(await LevelSandbox.update(block.height, JSON.stringify(block).toString())).to.equal(true);
      }
      expect(await blockchain.validateChain()).to.equal(false);
    });
  });
});
